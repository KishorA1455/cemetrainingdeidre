package com.allstate.lambdasdemos;

public interface GreetingService {
    void sayMessage(String message);
}
